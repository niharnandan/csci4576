TASK 1

1. The 'man' command stands for manual page. The manual page for bash hs the following description -

Bash  is  an  sh-compatible  command language interpreter that executes
commands read from the standard input or from a file.  Bash also incor‐
porates useful features from the Korn and C shells (ksh and csh).

Bash  is  intended  to  be a conformant implementation of the Shell and
Utilities portion  of  the  IEEE  POSIX  specification  (IEEE  Standard
1003.1).  Bash can be configured to be POSIX-conformant by default.

2. No description for LD_LIBRARAY_PATH in man ld page.

3. command - 'usr$ echo $HOME'; value - '/home/hna1125'

TASK 2

4. READ

5. commands - 
	'usr$ module load intel'
	'usr$ echo $PATH'; value - /curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/bin/intel64:/curc/sw/gcc/5.4.0/bin:/usr/lpp/mmfs/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/opt/dell/srvadmin/bin
	'echo $LD_LIBRARY_PATH'; value - /curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/compiler/lib/intel64:/curc/sw/gcc/5.4.0/lib64
	The values change because we are now on the intel module on the compiler node. We are using a 		different processor and memory space.

6. Modules available generally -

----------------------------------- Compilers ------------------------------------
   gcc/6.1.0           intel/17.0.0 (m)      pgi/16.5
   intel/16.0.3 (m)    intel/17.4   (m,D)    pgi/18.4 (D)

---------------------------- Independent Applications ----------------------------
   R/3.3.0                 git/2.8.3                    qt/4.8.5
   R/3.4.3                 gnu_parallel/20160622        qt/5.6.0
   R/3.5.0        (D)      idl/8.5                      qt/5.9.1             (D)
   allinea/6.0.4  (m)      jdk/1.7.0                    ruby/2.3.1
   autotools/2.69          jdk/1.8.0             (D)    singularity/2.3.1
   cmake/3.5.2             julia/0.6.2                  singularity/2.3.2
   cmake/3.9.2    (D)      loadbalance/0.1              singularity/2.4.2
   cube/3.4.3              loadbalance/0.2       (D)    singularity/2.5.2    (D)
   cube/4.3.4     (D)      mathematica/9.0              subversion/1.8.16
   cuda/7.5.18    (g)      mathematica/11.1.0    (D)    subversion/1.10.2    (D)
   cuda/8.0.61    (g)      ncl/6.3.0                    tcltk/8.6.5
   cuda/9.0.176   (g)      papi/5.4.3                   tdom/0.8.3
   cuda/9.1.85    (g,D)    papi/5.5.1            (D)    totalview/2016.06.21
   cudnn/4.0      (g)      paraview/5.0.1               udunits/2.2.20
   cudnn/5.1      (g)      pdtoolkit/3.22               udunits/2.2.24
   cudnn/7.1      (g,D)    perl/5.24.0                  udunits/2.2.25       (D)
   emacs/25.3              python/2.7.11         (D)    valgrind/3.11.0

Modules available on the intel node -

----------------------------- MPI Implementations ------------------------------
   impi/17.3

----------------------- Compiler Dependent Applications ------------------------
   antlr/2.7.7        intel_cluster_tools/17.3        netcdf/4.4.1.1
   fftw/3.3.4         intel_debugger/17.3             openjpeg/2.2.0
   gdal/2.2.1         jasper/1.900.1                  proj/4.9.2
   geos/3.6.2         jpeg/9b                         szip/2.1.1
   gsl/2.4            mkl/17.3                 (m)    zlib/1.2.11
   hdf5/1.8.18        nco/4.6.0
   hdf5/1.10.1 (D)    ncview/2.1.7

---------------------------------- Compilers -----------------------------------
   gcc/6.1.0           intel/17.0.0 (m)        pgi/16.5
   intel/16.0.3 (m)    intel/17.4   (m,L,D)    pgi/18.4 (D)

--------------------------- Independent Applications ---------------------------
   R/3.3.0                        mathematica/11.1.0   (D)
   R/3.4.3                        ncl/6.3.0
   R/3.5.0               (D)      papi/5.4.3
   allinea/6.0.4         (m)      papi/5.5.1           (D)
   autotools/2.69                 paraview/5.0.1

There are different modules available from within the intel module. I'm assuming these build on top of the intel module.

7. The reason behind using a hierarchial module system is that it provides layers to support programs built with specific compilers and also to provide library consistencies.

TASK 3

8. Environment variables set by -
	a. intel -
		AR=xiar
		MANPATH=/curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/man/common:/curc/sw/gcc/5.4.0/share/man:/curc/sw/lmod/lmod/share/man::
		XDG_SESSION_ID=66412
		_ModuleTable003_=c3cvbW9kdWxlcy9jdV9saWNlbnNlZCIsWyJ2ZXJzaW9uIl09Mix9
		HOSTNAME=shas0137
		INTEL_LICENSE_FILE=/curc/sw/intel/17.4/licenses/USE_SERVER.lic
		CURC_INTEL_BIN=/curc/sw/intel/17.4/bin
		TERM=xterm
		SHELL=/bin/bash
		HISTSIZE=1000
		LMOD_DEFAULT_MODULEPATH=/curc/sw/modules/compilers:/curc/sw/modules/idep:/curc/sw/lmod/lmod/modulefiles/Core:/curc/sw/modules/cu_licensed
		MODULEPATH_ROOT=/curc/sw/modules
		SSH_CLIENT=10.225.160.185 35376 22
		__LMOD_STACK_CXX=aWNwYw==
		CURC_INTEL_INC=/curc/sw/intel/17.4/include
		LMOD_PACKAGE_PATH=/curc/sw/modules/.site
		CURC_INTEL_ROOT=/curc/sw/intel/17.4
		LMOD_PKG=/curc/sw/lmod/lmod
		LMOD_VERSION=6.3.7
		__LMOD_STACK_LD=eGlsZA==
		MIC_LD_LIBRARY_PATH=/curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/mpirt/lib/ia32_lin:/curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/compiler/lib/mic
		SSH_TTY=/dev/pts/5
		__LMOD_STACK_FC=aWZvcnQ=
		__LMOD_STACK_CC=aWNj
		USER=hna1125
		LD_LIBRARY_PATH=/curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/compiler/lib/intel64:/curc/sw/gcc/5.4.0/lib64
		LS_COLORS=rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=01;05;37;41:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=01;36:*.au=01;36:*.flac=01;36:*.mid=01;36:*.midi=01;36:*.mka=01;36:*.mp3=01;36:*.mpc=01;36:*.ogg=01;36:*.ra=01;36:*.wav=01;36:*.axa=01;36:*.oga=01;36:*.spx=01;36:*.xspf=01;36:
		MIC_LIBRARY_PATH=/curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/mpirt/lib/ia32_lin:/curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/compiler/lib/mic
		LMOD_PREPEND_BLOCK=normal
		_ModuleTable001_=X01vZHVsZVRhYmxlXz17WyJhY3RpdmVTaXplIl09MSxiYXNlTXBhdGhBPXsiL2N1cmMvc3cvbW9kdWxlcy9jb21waWxlcnMiLCIvY3VyYy9zdy9tb2R1bGVzL2lkZXAiLCIvY3VyYy9zdy9sbW9kL2xtb2QvbW9kdWxlZmlsZXMvQ29yZSIsIi9jdXJjL3N3L21vZHVsZXMvY3VfbGljZW5zZWQiLH0sWyJjX3JlYnVpbGRUaW1lIl09ODY0MDAsWyJjX3Nob3J0VGltZSJdPTMuMjYwMjA2OTM3Nzg5OSxmYW1pbHk9e1siQ29tcGlsZXIiXT0iaW50ZWwiLH0saW5hY3RpdmU9e30sbVQ9e2ludGVsPXtbIkZOIl09Ii9jdXJjL3N3L21vZHVsZXMvY29tcGlsZXJzL2ludGVsLzE3LjQubHVhIixbImRlZmF1bHQiXT0xLFsiZnVsbE5hbWUiXT0iaW50ZWwvMTcuNCIsWyJsb2FkT3JkZXIiXT0x
		MAIL=/var/spool/mail/hna1125
		PATH=/curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/bin/intel64:/curc/sw/gcc/5.4.0/bin:/usr/lpp/mmfs/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/opt/dell/srvadmin/bin
		LD=xild
		CURC_INTEL_LIB=/curc/sw/intel/17.4/lib
		LMOD_SETTARG_CMD=:
		PWD=/home/hna1125
		_LMFILES_=/curc/sw/modules/compilers/intel/17.4.lua
		LANG=en_US.UTF-8
		MODULEPATH=/curc/sw/modules/mpis/intel/17.4:/curc/sw/modules/cdep/intel/17.4:/curc/sw/modules/compilers:/curc/sw/modules/idep:/curc/sw/lmod/lmod/modulefiles/Core:/curc/sw/modules/cu_licensed
		_ModuleTable_Sz_=3
		LOADEDMODULES=intel/17.4
		LMOD_CMD=/curc/sw/lmod/lmod/libexec/lmod
		LMOD_AVAIL_STYLE=grouped
		__LMOD_STACK_AR=eGlhcg==
		CXX=icpc
		HISTCONTROL=ignoredups
		SHLVL=1
		HOME=/home/hna1125
		_ModuleTable002_=LHByb3BUPXthcmNoPXtbIm1pYyJdPTEsfSx9LFsic2hvcnQiXT0iaW50ZWwiLFsic3RhdHVzIl09ImFjdGl2ZSIsfSx9LG1wYXRoQT17Ii9jdXJjL3N3L21vZHVsZXMvbXBpcy9pbnRlbC8xNy40IiwiL2N1cmMvc3cvbW9kdWxlcy9jZGVwL2ludGVsLzE3LjQiLCIvY3VyYy9zdy9tb2R1bGVzL2NvbXBpbGVycyIsIi9jdXJjL3N3L21vZHVsZXMvaWRlcCIsIi9jdXJjL3N3L2xtb2QvbG1vZC9tb2R1bGVmaWxlcy9Db3JlIiwiL2N1cmMvc3cvbW9kdWxlcy9jdV9saWNlbnNlZCIsfSxbInN5c3RlbUJhc2VNUEFUSCJdPSIvY3VyYy9zdy9tb2R1bGVzL2NvbXBpbGVyczovY3VyYy9zdy9tb2R1bGVzL2lkZXA6L2N1cmMvc3cvbG1vZC9sbW9kL21vZHVsZWZpbGVzL0NvcmU6L2N1cmMv
		FC=ifort
		BASH_ENV=/curc/sw/lmod/lmod/init/bash
		LOGNAME=hna1125
		CVS_RSH=ssh
		SSH_CONNECTION=10.225.160.185 35376 10.225.5.216 22
		MODULESHOME=/curc/sw/lmod/lmod
		PKG_CONFIG_PATH=/curc/sw/gcc/5.4.0/lib64/pkgconfig
		LESSOPEN=||/usr/bin/lesspipe.sh %s
		CURC_FAMILY_COMPILER=intel
		PROMPT_COMMAND=RETRN_VAL=$?;logger -p local6.debug "$(whoami) [$$]: $(history 1 | sed "s/^[ ]*[0-9]\+[ ]*//" ) [$RETRN_VAL]"
		LMOD_FAMILY_COMPILER=intel
		LMOD_FULL_SETTARG_SUPPORT=no
		CC=icc
		XDG_RUNTIME_DIR=/run/user/486989
		LMOD_DIR=/curc/sw/lmod/lmod/libexec
		LMOD_COLORIZE=yes
		BASH_FUNC_module()=() {  eval $($LMOD_CMD bash "$@");
		 [ $? = 0 ] && eval $(${LMOD_SETTARG_CMD:-:} -s sh)
		}
		BASH_FUNC_ml()=() {  eval $($LMOD_DIR/ml_cmd "$@")
		}
		_=/usr/bin/printen
	b. gcc -
		MANPATH=/curc/sw/gcc/6.1.0/share/man:/curc/sw/lmod/lmod/share/man::
		XDG_SESSION_ID=66412
		_ModuleTable003_=cnNpb24iXT0yLH0=
		HOSTNAME=shas0137
		TERM=xterm
		SHELL=/bin/bash
		HISTSIZE=1000
		LMOD_DEFAULT_MODULEPATH=/curc/sw/modules/compilers:/curc/sw/modules/idep:/curc/sw/lmod/lmod/modulefiles/Core:/curc/sw/modules/cu_licensed
		MODULEPATH_ROOT=/curc/sw/modules
		SSH_CLIENT=10.225.160.185 35376 22
		__LMOD_STACK_CXX=Zysr
		LMOD_PACKAGE_PATH=/curc/sw/modules/.site
		CURC_GCC_BIN=/curc/sw/gcc/6.1.0/bin
		LMOD_PKG=/curc/sw/lmod/lmod
		CURC_GCC_LIB=/curc/sw/gcc/6.1.0/lib
		LMOD_VERSION=6.3.7
		SSH_TTY=/dev/pts/5
		__LMOD_STACK_FC=Z2ZvcnRyYW4=
		__LMOD_STACK_CC=Z2Nj
		USER=hna1125
		LD_LIBRARY_PATH=/curc/sw/gcc/6.1.0/lib64
		LS_COLORS=rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=01;05;37;41:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=01;36:*.au=01;36:*.flac=01;36:*.mid=01;36:*.midi=01;36:*.mka=01;36:*.mp3=01;36:*.mpc=01;36:*.ogg=01;36:*.ra=01;36:*.wav=01;36:*.axa=01;36:*.oga=01;36:*.spx=01;36:*.xspf=01;36:
		CURC_GCC_INC=/curc/sw/gcc/6.1.0/include
		LMOD_PREPEND_BLOCK=normal
		_ModuleTable001_=X01vZHVsZVRhYmxlXz17WyJhY3RpdmVTaXplIl09MSxiYXNlTXBhdGhBPXsiL2N1cmMvc3cvbW9kdWxlcy9jb21waWxlcnMiLCIvY3VyYy9zdy9tb2R1bGVzL2lkZXAiLCIvY3VyYy9zdy9sbW9kL2xtb2QvbW9kdWxlZmlsZXMvQ29yZSIsIi9jdXJjL3N3L21vZHVsZXMvY3VfbGljZW5zZWQiLH0sWyJjX3JlYnVpbGRUaW1lIl09ODY0MDAsWyJjX3Nob3J0VGltZSJdPTMuMjYwMjA2OTM3Nzg5OSxmYW1pbHk9e1siQ29tcGlsZXIiXT0iZ2NjIix9LGluYWN0aXZlPXt9LG1UPXtnY2M9e1siRk4iXT0iL2N1cmMvc3cvbW9kdWxlcy9jb21waWxlcnMvZ2NjLzYuMS4wLmx1YSIsWyJkZWZhdWx0Il09MSxbImZ1bGxOYW1lIl09ImdjYy82LjEuMCIsWyJsb2FkT3JkZXIiXT0xLHByb3BU
		MAIL=/var/spool/mail/hna1125
		PATH=/curc/sw/gcc/6.1.0/bin:/usr/lpp/mmfs/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/opt/dell/srvadmin/bin
		LMOD_SETTARG_CMD=:
		PWD=/home/hna1125
		_LMFILES_=/curc/sw/modules/compilers/gcc/6.1.0.lua
		LANG=en_US.UTF-8
		MODULEPATH=/curc/sw/modules/mpis/gcc/6.1.0:/curc/sw/modules/cdep/gcc/6.1.0:/curc/sw/modules/compilers:/curc/sw/modules/idep:/curc/sw/lmod/lmod/modulefiles/Core:/curc/sw/modules/cu_licensed
		LOADEDMODULES=gcc/6.1.0
		_ModuleTable_Sz_=3
		LMOD_CMD=/curc/sw/lmod/lmod/libexec/lmod
		LMOD_AVAIL_STYLE=grouped
		CXX=g++
		HISTCONTROL=ignoredups
		SHLVL=1
		HOME=/home/hna1125
		CURC_GCC_ROOT=/curc/sw/gcc/6.1.0
		_ModuleTable002_=PXt9LFsic2hvcnQiXT0iZ2NjIixbInN0YXR1cyJdPSJhY3RpdmUiLH0sfSxtcGF0aEE9eyIvY3VyYy9zdy9tb2R1bGVzL21waXMvZ2NjLzYuMS4wIiwiL2N1cmMvc3cvbW9kdWxlcy9jZGVwL2djYy82LjEuMCIsIi9jdXJjL3N3L21vZHVsZXMvY29tcGlsZXJzIiwiL2N1cmMvc3cvbW9kdWxlcy9pZGVwIiwiL2N1cmMvc3cvbG1vZC9sbW9kL21vZHVsZWZpbGVzL0NvcmUiLCIvY3VyYy9zdy9tb2R1bGVzL2N1X2xpY2Vuc2VkIix9LFsic3lzdGVtQmFzZU1QQVRIIl09Ii9jdXJjL3N3L21vZHVsZXMvY29tcGlsZXJzOi9jdXJjL3N3L21vZHVsZXMvaWRlcDovY3VyYy9zdy9sbW9kL2xtb2QvbW9kdWxlZmlsZXMvQ29yZTovY3VyYy9zdy9tb2R1bGVzL2N1X2xpY2Vuc2VkIixbInZl
		FC=gfortran
		BASH_ENV=/curc/sw/lmod/lmod/init/bash
		LOGNAME=hna1125
		CVS_RSH=ssh
		SSH_CONNECTION=10.225.160.185 35376 10.225.5.216 22
		MODULESHOME=/curc/sw/lmod/lmod
		PKG_CONFIG_PATH=/curc/sw/gcc/6.1.0/lib64/pkgconfig
		LESSOPEN=||/usr/bin/lesspipe.sh %s
		CURC_FAMILY_COMPILER=gcc
		PROMPT_COMMAND=RETRN_VAL=$?;logger -p local6.debug "$(whoami) [$$]: $(history 1 | sed "s/^[ ]*[0-9]\+[ ]*//" ) [$RETRN_VAL]"
		LMOD_FAMILY_COMPILER=gcc
		LMOD_FULL_SETTARG_SUPPORT=no
		CC=gcc
		XDG_RUNTIME_DIR=/run/user/486989
		LMOD_DIR=/curc/sw/lmod/lmod/libexec
		LMOD_COLORIZE=yes
		BASH_FUNC_module()=() {  eval $($LMOD_CMD bash "$@");
		 [ $? = 0 ] && eval $(${LMOD_SETTARG_CMD:-:} -s sh)
		}
		BASH_FUNC_ml()=() {  eval $($LMOD_DIR/ml_cmd "$@")
		}
_=/usr/bin/printenv

TASK 4

9. Installed. Command - 'usr$ gcc -v'; output
	Using built-in specs.
	COLLECT_GCC=gcc
	COLLECT_LTO_WRAPPER=/usr/lib/gcc/x86_64-linux-gnu/7/lto-wrapper
	OFFLOAD_TARGET_NAMES=nvptx-none
	OFFLOAD_TARGET_DEFAULT=1
	Target: x86_64-linux-gnu
	Configured with: ../src/configure -v --with-pkgversion='Ubuntu 7.3.0-16ubuntu3' --with-bugurl=file:///usr/share/doc/gcc-7/README.Bugs --enable-languages=c,ada,c++,go,brig,d,fortran,objc,obj-c++ --prefix=/usr --with-gcc-major-version-only --with-as=/usr/bin/x86_64-linux-gnu-as --with-ld=/usr/bin/x86_64-linux-gnu-ld --program-suffix=-7 --program-prefix=x86_64-linux-gnu- --enable-shared --enable-linker-build-id --libexecdir=/usr/lib --without-included-gettext --enable-threads=posix --libdir=/usr/lib --enable-nls --with-sysroot=/ --enable-clocale=gnu --enable-libstdcxx-debug --enable-libstdcxx-time=yes --with-default-libstdcxx-abi=new --enable-gnu-unique-object --disable-vtable-verify --enable-libmpx --enable-plugin --enable-default-pie --with-system-zlib --with-target-system-zlib --enable-objc-gc=auto --enable-multiarch --disable-werror --with-arch-32=i686 --with-abi=m64 --with-multilib-list=m32,m64,mx32 --enable-multilib --with-tune=generic --enable-offload-targets=nvptx-none --without-cuda-driver --enable-checking=release --build=x86_64-linux-gnu --host=x86_64-linux-gnu --target=x86_64-linux-gnu
	Thread model: posix
	gcc version 7.3.0 (Ubuntu 7.3.0-16ubuntu3)

10. Command for compiling object file - 'usr$ gcc -c -fopenmp axpy.cc' (Similar for test file)
Command for compiling executable - 'usr$ g++ -o test_axpy axpy.o test_axpy.o -fopenmp'

TASK 5

11. Done. Should we submit the file? I've pushed the file either way.

TASK 6

12. Didn't get any warning.

13. Output timing for different number of threads - the optimum is around 4 threads. After that, there is a decrease in performance becase threads are being created and destroyed before they can perform any function.

usr$ ./test_axpy
Calling axpy - Passed
Elapsed time: 0.844853s
usr$ OMP_NUM_THREADS=2 ./test_axpy
Calling axpy - Passed
Elapsed time: 0.912536s
usr$ OMP_NUM_THREADS=1 ./test_axpy
Calling axpy - Passed
Elapsed time: 0.919466s
usr$ OMP_NUM_THREADS=4 ./test_axpy
Calling axpy - Passed
Elapsed time: 0.91819s
usr$ OMP_NUM_THREADS=8 ./test_axpy
Calling axpy - Passed
Elapsed time: 0.893909s
usr$ OMP_NUM_THREADS=16 ./test_axpy
Calling axpy - Passed
Elapsed time: 1.02416s

TASK 7

14. DONE

15. DONE

16. DONE

17. DONE