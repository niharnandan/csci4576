/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 7 $
 * @tags $Tags: tip $
 * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 *
 * @brief Tester for axpy
 */

#include "axpy.h"
#include <cstdlib>
#include <cmath>
#include <iostream>

#ifdef _OPENMP
#include <omp.h>
#define NOW()(omp_get_wtime())
#else
#define NOW()(0)
#endif
typedef double mytime_t;

int main(int, char**){

    int n(1e8);
    double *x = new double[n];
    double *y = new double[n];
    double a(1.0);

    std::cout<<"Calling axpy";
    mytime_t start=NOW();

    try {
        axpy(n,a,x,y);
        std::cout<<" - Passed"<<std::endl;
    }
    catch (...) {std::cout<<" - Failed"<<std::endl;}

    mytime_t end=NOW();
#ifdef _OPENMP
    std::cout<<"Elapsed time: "<<end-start<<"s"<<std::endl;
#endif

    delete[] x;
    delete[] y;

    return 0;
}
