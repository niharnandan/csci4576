#include "histogram.h"
#include <cstddef>
#include <vector>
#include <iostream>
#include <algorithm>

void histogram(const vec_t &x, int num_bins, vec_t &bin_bdry, count_t &bin_count)
{
	double min_value = x[0], max_value = x[0] ;
	#pragma omp parallel for reduction(max: max_value), reduction(min: min_value)
	for (unsigned int i = 1; i < x.size(); i++)
	{
		if (x[i] < min_value) min_value = x[i] ;
		if (x[i] > max_value) max_value = x[i] ;
	}
	double bin = (max_value - min_value)/num_bins ;
	bin_bdry.resize(num_bins+1, 0) ;
	bin_bdry[0] = (min_value*(1-1E-14)) ;
	#pragma omp parallel for
	for (unsigned int i = 1; i <= num_bins; i++)
	{
		bin_bdry[i] = ((min_value+bin*i)*(1+1E-14)) ;
	}

	double step = (bin_bdry[num_bins] - bin_bdry[0])/ num_bins ;
	
	#pragma omp declare reduction(vec_size_t_plus : std::vector<size_t> : \
                              std::transform(omp_out.begin(), omp_out.end(), 		omp_in.begin(), omp_out.begin(), std::plus<size_t>())) \
                    initializer(omp_priv=omp_orig)
        
        //std::vector<size_t>(num_bins, 0).swap(bin_count);
        bin_count.resize(num_bins,0);
	//bin_count = {0,0,0};
	
        #pragma omp parallel for reduction(vec_size_t_plus : bin_count)       
	for (unsigned int i = 0; i < x.size(); i++)
	{
		bin_count[int((x[i]-bin_bdry[0])/step)]++ ;
	}
	
	return  ;
}
