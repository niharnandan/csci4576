#!/bin/sh

time OMP_NUM_THREADS=2 ./a.out 2 1
time OMP_NUM_THREADS=2 ./a.out 5 3
time OMP_NUM_THREADS=2 ./a.out 9 4
time OMP_NUM_THREADS=4 ./a.out 2 1
time OMP_NUM_THREADS=4 ./a.out 5 3
time OMP_NUM_THREADS=4 ./a.out 9 4
time OMP_NUM_THREADS=8 ./a.out 2 1
time OMP_NUM_THREADS=8 ./a.out 5 3
time OMP_NUM_THREADS=8 ./a.out 9 4
time OMP_NUM_THREADS=16 ./a.out 2 1
time OMP_NUM_THREADS=16 ./a.out 5 3
time OMP_NUM_THREADS=16 ./a.out 9 4
time OMP_NUM_THREADS=32 ./a.out 2 1
time OMP_NUM_THREADS=32 ./a.out 5 3
time OMP_NUM_THREADS=32 ./a.out 9 4

