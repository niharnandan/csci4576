/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 3 $
 * @tags $Tags:  $
 * @date $Date: Tue Sep 26 23:15:22 2017 -0600 $
 *
 * @brief Tester for histogram
 */

#include "histogram.h"
#include "omp-utils.h"
#include <cstdio>
#include <cmath>   //power
#include <cstdlib> //rand
#include <cassert>

#include <cstddef>
#include <vector>
#include <iostream>
#include <algorithm>

//omp_set_num_threads(4);

void rand_fill(vec_t &x){

    for (auto &el : x)
        el = 1e-6*(rand() % 1000000); /* between 0 and 1 */
}

void histogram(const vec_t &x, int num_bins, vec_t &bin_bdry, count_t &bin_count)
{
	double min_value = x[0], max_value = x[0] ;
	#pragma omp parallel for reduction(max: max_value), reduction(min: min_value)
	for (unsigned int i = 1; i < x.size(); i++)
	{
		if (x[i] < min_value) min_value = x[i] ;
		if (x[i] > max_value) max_value = x[i] ;
	}
	double bin = (max_value - min_value)/num_bins ;
	bin_bdry.resize(num_bins+1, 0) ;
	bin_bdry[0] = (min_value*(1-1E-14)) ;
	#pragma omp parallel for
	for (unsigned int i = 1; i <= num_bins; i++)
	{
		bin_bdry[i] = ((min_value+bin*i)*(1+1E-14)) ;
	}

	double step = (bin_bdry[num_bins] - bin_bdry[0])/ num_bins ;
	
	#pragma omp declare reduction(vec_size_t_plus : std::vector<size_t> : \
                              std::transform(omp_out.begin(), omp_out.end(), 		omp_in.begin(), omp_out.begin(), std::plus<size_t>())) \
                    initializer(omp_priv=omp_orig)
        
        //std::vector<size_t>(num_bins, 0).swap(bin_count);
        bin_count.resize(num_bins,0);
	//bin_count = {0,0,0};
	
        #pragma omp parallel for reduction(vec_size_t_plus : bin_count)       
	for (unsigned int i = 0; i < x.size(); i++)
	{
		bin_count[int((x[i]-bin_bdry[0])/step)]++ ;
	}
	
	return  ;
}

int main(int argc, char** argv){

    // repeatable test
    srand(2018);

    // commandline arguments
    if (argc<3){
        printf("Log10 of array size and number of bins are mandatory arugments for %s.\n", __FILE__);
        exit(1);
    }

    size_t n(std::pow(10,atoi(argv[1])));
    size_t num_bins(std::pow(10,atoi(argv[2])));
    double outlier(num_bins);
    if (argc>3) outlier=atof(argv[3]);

    vec_t x(n), bin_bdry;
    count_t bin_count;
    rand_fill(x);
    x[n-1] = outlier;

    int nt = omp_get_max_threads();
    printf("Histogram of a random array of size %d, num_bins=%d, num_threads=%d\n",n,num_bins,nt);
    double tic(NOW());
    histogram(x, num_bins, bin_bdry, bin_count);
    double toc(NOW());
    printf("Elapsed time=%-5.2e\n",toc-tic);

    size_t nn(0);
    for (int iB=0;iB<num_bins;++iB){
        nn += bin_count[iB];
        printf("[bin %d: %5.2e -- %5.2e] %d\n", iB, bin_bdry[iB],bin_bdry[iB+1],bin_count[iB]);
    }
    assert (nn==n);

}
