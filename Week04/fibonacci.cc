#include <stdio.h>
#include <iostream>

long fib(long n) {
    long i(0), j(0);
    if (n<2) return 1;

    #pragma omp task shared(i)
    i = fib(n-1);

    #pragma omp task shared(j)
    j = fib(n-2);

    #pragma omp taskwait
    return i + j;
}

int main(int argc, char** argv)
{
	if (argc<2)
	{
		printf("Number to calculate fibonacci sum is mandatory %s.\n", __FILE__);
		exit(1);
    	}
    	long n(atoi(argv[1])), v;
	#pragma omp parallel shared(n, v)
	{	
		#pragma omp single
		v = fib(n);
	}
    	printf("Fibonacci sum %d.\n", v);
	return 0;
}
