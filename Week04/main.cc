/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision$
 * @tags $Tags$
 * @date $Date$
 *
 * @brief
 */

#include "message.h"
#include <cstdlib>

int main(){
  message m;
  m.printMessage();

  return 0;
}
