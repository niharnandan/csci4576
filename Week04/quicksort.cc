#include <iostream>
#include <vector>
#include "quicksort.h"

void swap(long* a, long* b)
{
	long temp = *a ;
	*a = *b ;
	*b = temp ;
	return ;
}

long partition (vec_t &arr, long low, long high)
{
	long pivot = arr[high];
	if (arr.size() >= 100000000-1) pivot = arr[(high-low)/2]
	long i = (low - 1);
	#pragma omp parallel for
	for (unsigned int j = low; j < high;j++)
	{
		if (arr[j] <= pivot)
		{
			#pragma omp atomic
			i++ ;
			swap(&arr[i], &arr[j]) ;
		}
	}
	swap(&arr[i+1], &arr[high]) ;
	return (i+1) ;
}

void QuickSort (vec_t &x, long low, long high)
{
	if (low < high)
	{
		int pi = partition(x, low, high) ;
		QuickSort (x, low, pi-1);
		QuickSort (x, pi+1, high);
	}
}

void quicksort(const vec_t &x, vec_t &y)
{
	y = x ;
	long low = 0, high = x.size()-1;
	QuickSort(y, low, high);

}

int main()
{
	const vec_t x = {4,3,2,0};
	vec_t y = {0,0,0,0} ;
	quicksort(x,y) ;
	for (auto i = y.begin(); i != y.end(); ++i)
    		std::cout << *i << ' ';
	return 0;
}
