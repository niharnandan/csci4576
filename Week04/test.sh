#!/bin/bash

for j in 24000000 240000000
do 
	for i in 1 2 4 8 16 32
	do
		OMP_NUM_THREADS=$i ./test_quicksort.exe $j
	done
done
