#!/bin/bash
 
for i in 1000000 10000000
do
	for j in 1 2 4 8 16
	do
		OMP_NUM_THREADS=$j ./test_quicksort.exe $(($i*$j))
	done
done
