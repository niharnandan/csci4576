/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 3 $
 * @tags $Tags:  $
 * @date $Date: Tue Sep 26 23:15:22 2017 -0600 $
 *
 */

#include "quicksort.h"
#include <iostream>
#include <cmath>
#include <cassert>
#include "omp-utils.h"
#include <algorithm>
#include <cstdio>
#include <vector>

void rand_fill(vec_t &x){

    for (auto &el : x)
        el = rand();
}

void swap(long* a, long* b)
{
	long temp = *a ;
	*a = *b ;
	*b = temp ;
	return ;
}

long partition (vec_t &arr, long low, long high)
{
	long pivot = arr[high];
	long i = (low - 1);
	for (unsigned int j = low; j < high;j++)
	{
		if (arr[j] <= pivot)
		{
			i++ ;
			swap(&arr[i], &arr[j]) ;
		}
	}
	swap(&arr[i+1], &arr[high]) ;
	return (i+1) ;
}

void QuickSort (vec_t &x, long low, long high)
{
	if (low < high)
	{
		int pi = partition(x, low, high) ;
		#pragma omp task shared(x)
		QuickSort (x, low, pi-1);
		#pragma omp task shared(x)
		QuickSort (x, pi+1, high);
	}
}

void quicksort(const vec_t &x, vec_t &y)
{
	y = x ;
	long low = 0, high = x.size()-1;
	#pragma omp parallel
	#pragma omp single
	QuickSort(y, low, high);

}

int main(int argc, char** argv){

    // repeatable test
    srand(2017);

    // commandline arguments
    if (argc<2){
        printf("Array size is a mandatory arugments for %s.\n", __FILE__);
        exit(1);
    }

    size_t n(atoi(argv[1]));
    printf("Sorting a random array of size %dK, num_threads=%d\n",n/1000,omp_get_max_threads());
    vec_t x(n),y(n);
    rand_fill(x);

    double tic=NOW();
    quicksort(x,y);
    double toc=NOW();
    printf("Elapsed=%5.2e\n",toc-tic);

    // checking
    std::sort(x.begin(),x.end());
    for (size_t ii=0;ii<x.size();++ii)
        assert (x[ii]==y[ii]);

}
