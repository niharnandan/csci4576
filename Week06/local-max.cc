#include <mpi.h>
#include "slurp_file.cc"
#include <vector>
#include <string>

int main(int argc, char *argv[])
{
	if (argc < 2) exit(1) ;
	char *name(argv[1]) ;
	std::vector<int> data; 
	int rank, nproc; 

	MPI_Init(NULL, NULL) ;
	MPI_Comm_size(MPI_COMM_WORLD, &nproc);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Status st ;
	slurp_file_line(name, rank, data);
	int maxe = 0 ;
	std::vector<int> g_max(4, 0) ;
	for (unsigned int i = 0; i < data.size() ; i++)
		if (data[i] > maxe)
			maxe = data[i];
	if (rank != 0)
		MPI_Send(&maxe, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);	
	else
	{
		g_max[0] = maxe ;
		for (unsigned int i = 1; i < 4; i++)
			MPI_Recv(&g_max[i], 1, MPI_INT, i, 0, MPI_COMM_WORLD, &st);
		int temp = 0;
		for (unsigned int i = 0; i < 4 ; i++)
			if (g_max[i] > temp)
				temp = g_max[i] ;
		std::cout << temp << '\n' ;
	}
	return 0;

}
