#include <iostream>
#include <mpi.h>

int main(int argc, char** argv){

    MPI_Init(&argc,&argv);
    int rank,nproc;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nproc); 
    std::cout << "Hello from processor " << rank << " of " << nproc << "\n";	
    
    MPI_Finalize();
}
