#include <mpi.h>
#include "slurp_file.cc"
#include <vector>
#include <string>

int main(int argc, char *argv[])
{
	if (argc < 2) exit(1) ;
	char *name(argv[1]) ;
	std::vector<int> col, vec; 
	int rank, nproc; 

	MPI_Init(NULL, NULL) ;
	MPI_Comm_size(MPI_COMM_WORLD, &nproc);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Request re;
	MPI_Status st;
	//MPI_Status st ;
	slurp_file_line(name, rank, col);
	slurp_file_line(name, nproc, vec);
	int sum[col.size()] ;
	
	for (unsigned int i = 0; i < col.size() ; i++)
		sum[i] = col[i] * vec[rank];
	if (rank == nproc - 1)
		MPI_Send(&sum, col.size(), MPI_INT, rank-1, 0, MPI_COMM_WORLD);
	else if (rank != 0)
	{
		int *temp = new int[col.size()];
		MPI_Recv(temp, col.size(), MPI_INT, rank+1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		int final_s[col.size()];
		for (unsigned int i = 0; i < col.size(); i++)
			final_s[i] = sum[i] + temp[i] ;
		MPI_Send(&final_s, col.size(), MPI_INT, rank-1, 0, MPI_COMM_WORLD);
	}
	else
        {
		int temp[col.size()];
                MPI_Recv(&temp, col.size(), MPI_DOUBLE, 1, 0, MPI_COMM_WORLD, &st);
                std::vector<int> final_s (col.size(), 0) ;
                for (unsigned int i = 0; i < col.size(); i++)
                        final_s[i] = sum[i] + temp[i] ;
		for (unsigned int i = 0; i < col.size()-1; i++)
			std::cout << final_s[i] << ' ' ;
		std::cout << '\n' ;
        }
	MPI_Finalize();
	return 0;
}
