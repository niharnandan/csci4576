#!/bin/bash

for i in 1 2 4 8 12 16 20 24
do
	time OMP_NUM_THREADS=$i ./test_histogram.exe 2 1 
done

for i in 1 2 4 8 12 16 20 24
do

	time OMP_NUM_THREADS=$i ./test_histogram.exe 5 1 
done


for i in 1 2 4 8 12 16 20 24
do

	time OMP_NUM_THREADS=$i ./test_histogram.exe 9 1 
done


for i in 1 2 4 8 12 16 20 24
do
	time OMP_NUM_THREADS=$i ./test_histogram.exe 2 1 2
done

for i in 1 2 4 8 12 16 20 24
do

	time OMP_NUM_THREADS=$i ./test_histogram.exe 5 1 2
done


for i in 1 2 4 8 12 16 20 24
do

	time OMP_NUM_THREADS=$i ./test_histogram.exe 9 1 2
done
