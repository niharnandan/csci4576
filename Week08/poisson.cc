/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 25 $
 * @tags $Tags: tip $
 * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 */

#include "poisson.h"
#include <iostream>

void poisson_setup(const MPI_Comm &comm, int n, MPI_Comm &grid_comm,grid_t &x)
{
	int size;
	int err = MPI_Comm_size(comm, &size);

	//Error handling
	if (err != MPI_SUCCESS) 
	{
   		printErrorMessage(err);i
   		return;
	}
    
	int dims[3] = {0, 0, 0};
	err = MPI_Dims_create(size, 3, dims);
    
	//Error handling
	if (err != MPI_SUCCESS)
	{
		printErrorMessage(err);
		return;
	}
	static const int periodic [ ] = {0, 0, 0};
	err =  MPI_Cart_create(comm, 3, dims, periodic, 1, &grid_comm);
	//Error handling
	if(err != MPI_SUCCESS)
	{
		printErrorMessage(err);
		return;
	}
	//Get grid coordinates
   	int rank;
	err = MPI_Comm_rank(grid_comm, &rank);
 	int coords[3];
	err =  MPI_Cart_coords(grid_comm, rank, 3, coords);
	//Error handling
	if(err != MPI_SUCCESS)
	{	
 		printErrorMessage(err);
		eturn;
	}	
//printf("Rank %d: %d, %d, %d\n", rank, coords[0], coords[1], coords[2]);
//Assign sample points
	double i_interval = ceil((double)n / (double)dims[0]);
	double j_interval = ceil((double)n / (double)dims[1]);
	double k_interval = ceil((double)n / (double)dims[2]);
	for(real_t i=coords[0]*i_interval; i< min((coords[0]+1)*i_interval, n); i++)
	{
		for(real_t j=coords[1]*j_interval; j<min((coords[1]+1)*j_interval, n); j++)
		{
			for(real_t k=coords[2]*k_interval; k< min((coords[2]+1)*k_interval, n); k++)
			{
			    point_t pt = {i, j, k};  
				x.push_back(pt);
			}
		}
	}
}

void poisson_matvec(const MPI_Comm &grid_comm, int n, const vec_t &a, const vec_t &v, vec_t &lv)
{
    std::cout<<"matvec"<<std::endl;
}

void poisson_residual(MPI_Comm &grid_comm, std::vector<point_t> &x, std::vector<real_t> &a, std::vector<real_t> &f,
                      std::vector<real_t> &v, real_t &res, int n)
{

    MPI_Status st;

    point_t upper_left = x.front();
    point_t lower_right = x.back();

    int i_interval = lower_right.x - upper_left.x;
    int j_interval = lower_right.y - upper_left.y;
    int k_interval = lower_right.z - upper_left.z;

    int rank;
    int err = MPI_Comm_rank(grid_comm, &rank);

/**/
    for(int i=0; i<i_interval; i++)
	{
        for(int j=0; j<j_interval; j++)
		{
            for(int k=0; k<k_interval; k++)
			{
                real_t Lv = 0;

                int idx = sub2ind(i, j, k, i_interval, j_interval, k_interval);

                real_t v_1 = v[idx];
                real_t a_1 = a[idx];
                real_t h = 1.0/((double)n-1.0);

                if(i==0) 
				{
                    Lv += shift_grid(st, grid_comm, v_1, 0, -1);
                    Lv += v[sub2ind(i+1, j, k, i_interval, j_interval, k_interval)] - v_1;
                }
				else if(i==i_interval-1)
				{
                    Lv += shift_grid(st, grid_comm, v_1, 0, 1);
                    Lv += v[sub2ind(i-1, j, k, i_interval, j_interval, k_interval)] - v_1;
                }
				else
				{
                    Lv += v[sub2ind(i+1, j, k, i_interval, j_interval, k_interval)] - v_1;
                    Lv += v[sub2ind(i-1, j, k, i_interval, j_interval, k_interval)] - v_1;
                }

                if(j==0)
					{
                    Lv += shift_grid(st, grid_comm, v_1, 1, -1);
                    Lv += v[sub2ind(i, j+1, k, i_interval, j_interval, k_interval)] - v_1;
                }
				else if(j==j_interval-1)
				{
                    Lv += shift_grid(st, grid_comm, v_1, 1, 1);
                    Lv += v[sub2ind(i, j-1, k, i_interval, j_interval, k_interval)] - v_1;
                }
				else
				{
                    Lv += v[sub2ind(i, j+1, k, i_interval, j_interval, k_interval)] - v_1;
                    Lv += v[sub2ind(i, j-1, k, i_interval, j_interval, k_interval)] - v_1;
                }

                if(k==0) 
				{
                    Lv += shift_grid(st, grid_comm, v_1, 2, -1);
                    Lv += v[sub2ind(i, j, k+1, i_interval, j_interval, k_interval)] - v_1;
                }
				else if(k==k_interval-1)
				{
                    Lv += shift_grid(st, grid_comm, v_1, 2, 1);
                    Lv += v[sub2ind(i, j, k-1, i_interval, j_interval, k_interval)] - v_1;
                }
				else
				{
                    Lv += v[sub2ind(i, j, k+1, i_interval, j_interval, k_interval)] - v_1;
                    Lv += v[sub2ind(i, j, k-1, i_interval, j_interval, k_interval)] - v_1;
                }

                Lv = a_1*v_1 - Lv/h;
                res += (f[idx] - Lv)*(f[idx] - Lv);

            }
        }
    }

    if(reduce(st, grid_comm, res, 0, -1)) 
        if (reduce(st, grid_comm, res, 1, -1)) 
            reduce(st, grid_comm, res, 2, -1);

}

int min(int a, int b)
{
    return !(b<a)?a:b;
}

int sub2ind(int x, int y, int z, int i_interval, int j_interval, int k_interval)
{
    return z + y*j_interval + x*j_interval*i_interval;
}

real_t shift_grid(MPI_Status st, MPI_Comm &grid_comm, real_t v, int dim, int dir){
    int rank_source, rank_dest;
    int err = MPI_Cart_shift(grid_comm, dim, 1*dir, &rank_source, &rank_dest);

    if(err != MPI_SUCCESS)
	{
        printErrorMessage(err);
        return 0;
    }

    if (rank_dest != MPI_PROC_NULL)
        MPI_Send(&v, 1, MPI_DOUBLE, rank_dest, 0, grid_comm);

    err = MPI_Cart_shift(grid_comm, dim, -1*dir, &rank_source, &rank_dest);

    if(err != MPI_SUCCESS)
	{
        printErrorMessage(err);
        return 0;
    }

    if (rank_dest != MPI_PROC_NULL) 
	{
        real_t left;
        MPI_Recv(&left, 1, MPI_DOUBLE, rank_dest, 0, grid_comm, &st);

        return left - v;
    }

    return 0;
}

bool reduce(MPI_Status st, MPI_Comm &grid_comm, real_t &val, int dim, int dir)
{
    int rank_source, rank_dest;
    int err = MPI_Cart_shift(grid_comm, dim, -1 * dir, &rank_source, &rank_dest);

    if (err != MPI_SUCCESS) 
	{
        printErrorMessage(err);
    } 
	else if(rank_dest!= MPI_PROC_NULL) 
	{
        real_t left = 0;
        MPI_Recv(&left, 1, MPI_DOUBLE, rank_dest, 0, grid_comm, &st);
        val += left;
    }

    //Send Left
    err = MPI_Cart_shift(grid_comm, dim, 1 * dir, &rank_source, &rank_dest);

    if (err != MPI_SUCCESS) 
	{
        printErrorMessage(err);
    }
	else if(rank_dest != MPI_PROC_NULL) 
	{
        MPI_Send(&val, 1, MPI_DOUBLE, rank_dest, 0, grid_comm);
        return false;
    }

    return true;
}
