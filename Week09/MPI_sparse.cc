#include "MPI_sparse.h"
#include <mpi.h>
#include <cmath>

sparse::sparse(size_t m, size_t n, storage_type t) :
        m(m), n(n), type(t) {}

void sparse::transpose(sparse &B) const
{
    int size = 1, rank = 0;
	MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    toCOO(B);

    std::vector<size_t> tmp_row = B.row;
    B.row = B.column;
    B.column = tmp_row;

    int newM = ceil(B.n/size);
    std::vector<size_t> row_dest(B.row.size());
    for(int i=0; i<B.row.size(); i++)
        row_dest[i] = B.row[i]/newM;

    std::vector<size_t> index;
    sort_index(row_dest, index);
    size_t send_row[B.row.size()];
    size_t send_col[B.column.size()];
    data_t send_data[B.data.size()];

    std::vector<int> send_counts(size, 0);
    std::vector<int> send_displace(size, 0);

    for(int i=0; i<index.size(); i++)
	{
        send_row[i] = B.row[index[i]]%newM;
        send_col[i] = B.column[index[i]]+rank*B.m;
        send_data[i] = B.data[index[i]];
        send_counts[row_dest[index[i]]] += 1;
    }

    send_displace[0] = 0;
    for(int i=1; i<index.size(); i++)
        send_displace[i] = send_displace[i-1]+send_counts[i-1];

    int rec_counts[size];
    MPI_Alltoall(send_counts.data(), 1, MPI_INT, rec_counts, 1, MPI_INT, MPI_COMM_WORLD);

    int rec_displace[size];
    int total = 0;
    for(int i=0; i<size; i++)
	{
        rec_displace[i] = total;
        total += rec_counts[i];
    }

    std::vector<size_t> rec_row(total);
    std::vector<size_t> rec_col(total);
    std::vector<data_t> rec_data(total);

    MPI_Alltoallv(send_row, send_counts.data(), send_displace.data(), my_MPI_SIZE_T,
	rec_row.data(), rec_counts, rec_displace, my_MPI_SIZE_T, MPI_COMM_WORLD);

    MPI_Alltoallv(send_col, send_counts.data(), send_displace.data(), my_MPI_SIZE_T,
    rec_col.data(), rec_counts, rec_displace, my_MPI_SIZE_T, MPI_COMM_WORLD);

    MPI_Alltoallv(send_data, send_counts.data(), send_displace.data(), MPI_DOUBLE,
    rec_data.data(), rec_counts, rec_displace, MPI_DOUBLE, MPI_COMM_WORLD);

    B.row = rec_row;
    B.column = rec_col;
    B.data = rec_data;

    sparse C(newM, this->m*size, sparse::COO);
    sort_index(B.row, index);
    C.row = std::vector<size_t>(B.row.size());
    C.column = std::vector<size_t>(B.column.size());
    C.data = std::vector<data_t>(B.data.size());
    for (std::size_t i = 0; i < index.size(); i++) 
	{
        C.column[i] = B.column[index[i]];
        C.row[i] = B.row[index[i]];
        C.data[i] = B.data[index[i]];
    }

    C.toCSR(B); /**/
}

void sparse::toCOO(sparse &B) const
{
    B.type = sparse::COO;
    B.data = this->data;
    B.column = this->column;
    B.m = this->m;
    B.n = this->n;
    if(this->type==sparse::COO)
        B.row = this->row;
    else 
	{
        B.row = std::vector<size_t>();
        size_t prev = 0;
        for(int i=0; i<this->row.size(); i++) 
		{
            for(int j=0; j < this->row[i] - prev; j++)
			{
                B.row.push_back(i-1);
            }
            prev = this->row[i];
        }
    }
}

void sparse::toCSR(sparse &B) const{
    B.type = sparse::CSR;
    B.data = this->data;;
    B.column = this->column;
    B.m = this->m;
    B.n = this->n;
    if(this->type==sparse::CSR) 
        B.row = this->row;
    else 
	{
        B.row = std::vector<size_t>();
        B.row.push_back(0);
        size_t prev = 0, total = 0;
        for(int i=0; i<this->row.size(); i++) 
		{
            if(this->row[i]!=prev)
			{
                for(int j = B.row.size(); j<=row[i]; j++)
                    B.row.push_back(total);
                prev = this->row[i];
            }
            total++;
        }

        for(int i = B.row.size(); i < B.m+1; i++)
            B.row.push_back(B.row[0]+B.data.size());
    }
}

void sparse::toString(std::string &s)
{
    std::string row_s;
    vectorToString(this->row, row_s);
    std::string col_s;
    vectorToString(this->column, col_s);
    std::string data_s;
    vectorToString(this->data, data_s);
    s = "row: " + row_s +"\ncol: " + col_s + "\ndata: " + data_s;
}

void sort_index(const std::vector<size_t> &v, std::vector<size_t> &idx)
{
    size_t n(0);
    idx.resize(v.size());
    std::generate(idx.begin(), idx.end(), [&]{ return n++; });
    std::sort(idx.begin(), idx.end(), [&](size_t ii, size_t jj) { return v[ii] < v[jj]; } );
}
