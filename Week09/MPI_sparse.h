#ifndef W07_SPARSE_H
#define W07_SPARSE_H

#include <stdint.h>
#include <limits.h>

#if SIZE_MAX == UCHAR_MAX
#define my_MPI_SIZE_T MPI_UNSIGNED_CHAR
#elif SIZE_MAX == USHRT_MAX
#define my_MPI_SIZE_T MPI_UNSIGNED_SHORT
#elif SIZE_MAX == UINT_MAX
   #define my_MPI_SIZE_T MPI_UNSIGNED
#elif SIZE_MAX == ULONG_MAX
   #define my_MPI_SIZE_T MPI_UNSIGNED_LONG
#elif SIZE_MAX == ULLONG_MAX
   #define my_MPI_SIZE_T MPI_UNSIGNED_LONG_LONG
#endif

#include <vector>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <string>

typedef double data_t;

void sort_index(const std::vector<size_t> &v, std::vector<size_t> &idx);

class sparse 
{
	public:
	enum storage_type {CSR, COO};
	std::vector<data_t> data;
	std::vector<size_t> column;
	std::vector<size_t> row;

	size_t m,n;
	storage_type type;
	sparse(size_t m, size_t n, storage_type t = CSR);
	void transpose(sparse &B) const;
	void toCOO(sparse &B) const; 
	void toCSR(sparse &B) const; 
	void toString(std::string &s);
};

template <class T>
void vectorToString(std::vector<T> x, std::string &s)
{
    std::stringstream ss;
    for (int i = 0; i<x.size(); i++)
        ss << x[i] << ' ';
    s = ss.str();
}

template <class T>
void arrayToString(T x[], int n, std::string &s)
{
    std::stringstream ss;
    for (int i = 0; i<n; i++)
        ss << x[i] << ' ';
    s = ss.str();
}
#endif //W07_SPARSE_H