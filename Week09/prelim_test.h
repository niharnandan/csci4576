#include <iostream>
#include <mpi.h>

int main(int argc, char* argv[])
{
    MPI_Init(&argc, &argv);
    int rank, nproc;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&nproc);

    int local_size(rank+1);
    int *local_data = new int[local_size];
    for (int ii=0;ii<local_size;++ii) local_data[ii]=rank;

    int bufsz;
    int *all_data(NULL);

    int *local_sizes(NULL), *offsets(NULL);
    if (rank == 0) 
	{
        local_sizes = new int[nproc];
        offsets     = new int[nproc];
    }

    MPI_Gather(&local_size, 1, MPI_INT, local_sizes,1, MPI_INT, 0, MPI_COMM_WORLD);

    if (rank == 0) 
	{
        offsets[0] = 0;
        for (int ii=1; ii<nproc; ++ii)
            offsets[ii] = offsets[ii-1]+local_sizes[ii-1];

        bufsz    = offsets[nproc-1]+local_sizes[nproc-1];
        all_data = new int[bufsz];
    }

    MPI_Gatherv(local_data,local_size, MPI_INT,
    all_data,local_sizes,offsets,MPI_INT, 0, MPI_COMM_WORLD);

    if (rank == 0)
        for (int ii=0; ii<bufsz; ++ii)
            printf("data[%d]=%d\n",ii,all_data[ii]);

    delete[] local_sizes;
    delete[] offsets;
    delete[] all_data;

    MPI_Finalize();

}