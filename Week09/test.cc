#include <iostream>
#include <mpi.h>
#include "MPI_sparse.h"
#include <algorithm>
#include <cstdlib>

void singleProcessorTest()
{
    sparse A(4, 4, sparse::CSR);
    int rows[] = {1,3,5,6,7};
    int cols[] = {2,3,4,5,6,7};
    int data[] = {10,11,12,13,14,15};
    std::copy(&rows[0], &rows[5], std::back_inserter(A.row));
    std::copy(&cols[0], &cols[6], std::back_inserter(A.column));
    std::copy(&data[0], &data[6], std::back_inserter(A.data));
    //std::cout << "A" << std::endl;
    std::string s;
    A.toString(s);
    std::cout << s << std::endl;
    sparse B(4, 4, sparse::CSR);
    A.transpose(B);
    //std::cout << "B" << std::endl;
    B.toString(s);
    std::cout << s << std::endl;
}

void randMat(MPI_Comm communicator, sparse &B)
{
    int rank;
    MPI_Comm_rank(communicator, &rank);
    sparse A(B.m, B.n, sparse::COO);
    for (int i = 0; i< A.m; i++){
        int col = rand()%A.n;
        A.column.push_back(col);
        A.row.push_back(i);
        A.data.push_back(rank*B.m+i+1);
    }
    A.toCSR(B);
}

int main(int argc, char* argv[])
{
    MPI_Init(&argc, &argv);
    int size = 0, rank = 0;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if(size == 1)
        singleProcessorTest();
    else{
        sparse A(size, size*size*2, sparse::CSR);
        randMat(MPI_COMM_WORLD, A);
        std::string s;
        A.toString(s);
        std::cout << "A rank: " << rank << std::endl << s << "\n\n";
        sparse B(size*size*2, size, sparse::CSR);
        A.transpose(B);
        B.toString(s);
        std::cout << "B rank: " << rank << std::endl << s << "\n\n";
    }
    MPI_Finalize();
}