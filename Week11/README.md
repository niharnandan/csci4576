This work is done on top of shane's solution from the previous labs.

With the given er and ea values and p = 24 (maximum number of processors)
Number of iterations -
1. n = 256
	~800 iterations
2. n = 512
	~4000 iterations
3. n = 758
	Limit is reached before u_final
4. n = 1024
	Limit is reached before u_final
	
k_max is set to 10000. The iterations are rounded off due to a small bug in my code and the final 
values are inconsistent. 