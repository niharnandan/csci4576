/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 25 $
 * @tags $Tags: tip $
 * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 *
 */

#include <vector>
#include <functional>
#include <mpi.h>
#include <cmath>

// Some utlity classes and typedefs
#define DIM 3

typedef double real_t;

union point_t
{
    real_t coord[3];
    struct {
        real_t x;
        real_t y;
        real_t z;
    };
};

typedef std::vector<real_t> vec_t;
typedef std::vector<point_t> grid_t;

// define a type for matvec with vector input and output
typedef std::function<void(vec_t &, vec_t &)> matvec_t;

/*
 * Set up the grid communicator for the Poisson problem and samples
 * the correct portion of the domain
 *
 * @comm input communicator, typically MPI_COMM_WORLD
 * @n problem size, there are n sample points along each axis
 * @grid_comm the grid communicator (output)
 * @x sample points assigned to the current MPI process based on its grid coordinates (output)
 *
 */
void poisson_setup(const MPI_Comm &comm, int n, MPI_Comm &grid_comm, grid_t &x);

/*
 * @grid_comm grid communicator returned by poisson_setup
 * @a value of coefficient a at points x
 * @v candidate solution at points x
 * @lv the matrix-vector product of the discrete Poisson's operator
 */
void poisson_matvec(const MPI_Comm &grid_comm, int n, const vec_t &a, vec_t &v, vec_t &lv);

/*
 * @comm mpi communicator
 * @mv the matvec operator
 * @v the input vector to matvec
 * @rhs the right-hand-side of the linear operator
 * @res the point-wise residual (output)
 * @res_norm the L2 norm of the residual vector `res`
 */
void residual(const MPI_Comm &comm, matvec_t &mv, vec_t &v, const vec_t &rhs, vec_t &res, real_t &res_norm);

/*
 * @comm MPI communicator (to compute residual)
 * @mv the matvec
 * @diag diagonal component of the matvec
 * @rhs the right hand side
 * @u0 initial guess
 * @eps_r relative tolerance
 * @eps_a absolute tolerance
 * @m check residual every m iterations
 * @k_max the maximum number of iteration
 * @u_final return value
 * @k the number of iteration taken
 */
 
void jacobi_solver(const MPI_Comm &comm, matvec_t &mv, 
                 std::vector<real_t> &diag, std::vector<real_t> &rhs,
                 std::vector<real_t> &u0, real_t eps_r, real_t eps_a, int k_max,
                 std::vector<real_t> &u_final, int &k);
				 
void poisson_diagonal(int n, const vec_t &a, vec_t &v, vec_t &D);
