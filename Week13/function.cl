__kernel void mandelbrot_cl(__global int *A, float xmin, float xmax,
	float ymin, float ymax, int n, int max_iter)
{  
        float pix_size = (ymax - ymin) / (float)n;
        // raw pixel coordinate (in the range 0 to n - 1 for x and y)
	int x_raw = get_global_id(0);
	int y_raw = get_global_id(1);
	
	// scaled pixel coordinate (in the range xmin to xmax and ymin to ymax)
	float x_scl = xmin + (x_raw * pix_size);
	float y_scl = ymin + (y_raw * pix_size);

	float x = 0;
	float y = 0;
	int iter = 0;

	while ((x*x + y*y <= 4) && (iter <= max_iter))
	{	
	      float x_temp = (x*x - y*y) + x_scl;
	      y = (2 * x * y) + y_scl;
	      x = x_temp;
	      iter = iter + 1;
	}

	A[((x_raw*n) + y_raw)] = iter;
}