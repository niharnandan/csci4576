#include <stdio.h>
#include <cstdlib>
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#include <time.h>
#define MAX_SOURCE_SIZE (0x100000)
#define work_dims 2
typedef float real_t;

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

void Mandelbrot (real_t xmin, real_t xmax, real_t ymin, real_t ymax, int n,	int max_iter, int *mandelbrot)
{
	FILE* file;
	char *source_string;
	size_t source_size;

	file = fopen("function.cl", "rb");

	if (!file)
	{
	    printf("File Mandelbrot.cl could not be opened.\n");
        exit(1);
	}

	source_string = (char*)malloc(MAX_SOURCE_SIZE);
	source_size = fread(source_string, 1, MAX_SOURCE_SIZE, file);
	fclose(file);

	cl_int ret;
	cl_uint num_Platforms;
	cl_uint num_Devices;
	cl_device_id device_ID = NULL;
	cl_platform_id platform_ID = NULL;
	ret = clGetPlatformIDs(1, &platform_ID, &num_Platforms);
	ret = clGetDeviceIDs(platform_ID, CL_DEVICE_TYPE_CPU, 1, &device_ID, &num_Devices);
	
	cl_context context = clCreateContext(NULL, 1, &device_ID, NULL, NULL, &ret);
	cl_command_queue command_queue = clCreateCommandQueue(context, device_ID, 0, &ret);

	cl_program program = clCreateProgramWithSource(context, 1,
	     (const char **)&source_string, (const size_t *)&source_size, &ret);
	ret = clBuildProgram(program, 1, &device_ID, NULL, NULL, NULL);
	size_t len = 0;
	ret = CL_SUCCESS;
	ret = clGetProgramBuildInfo(program, device_ID, CL_PROGRAM_BUILD_LOG, 0, NULL, &len);
	char *buffer = (char *)malloc(len * sizeof(char));
	ret = clGetProgramBuildInfo(program, device_ID, CL_PROGRAM_BUILD_LOG, len, buffer, NULL);
	printf("%s\n", buffer);
	free(buffer);
	cl_kernel mandelbrot_cl = clCreateKernel(program, "mandelbrot_cl", &ret);

	clock_t begin = clock();
	int buffer_size = n * n * sizeof(int);
	mandelbrot = (int *)malloc(n * n * sizeof(int *));
	cl_mem mem_array = clCreateBuffer(context, CL_MEM_WRITE_ONLY, buffer_size, NULL, &ret);
	
	// Passing in arguments to kernel function
	clSetKernelArg(mandelbrot_cl, 0, sizeof(cl_mem), (void*)&mem_array); 
	clSetKernelArg(mandelbrot_cl, 1, sizeof(real_t), (void*)&xmin);
	clSetKernelArg(mandelbrot_cl, 2, sizeof(real_t), (void*)&xmax);
	clSetKernelArg(mandelbrot_cl, 3, sizeof(real_t), (void*)&ymin);
	clSetKernelArg(mandelbrot_cl, 4, sizeof(real_t), (void*)&ymax);
	clSetKernelArg(mandelbrot_cl, 5, sizeof(int), (void*)&n);
	clSetKernelArg(mandelbrot_cl, 6, sizeof(int), (void*)&max_iter);

	// Setting global and local work sizes
	size_t *global_work_size = (size_t *)malloc(work_dims * sizeof(size_t));
	size_t *local_work_size = (size_t *)malloc(work_dims * sizeof(size_t));
	global_work_size[0] = n;
	global_work_size[1] = n;
	local_work_size[0] = 64;
	local_work_size[1] = 64;

	// Execute kernel function and read in memory object contents
	ret = clEnqueueNDRangeKernel(command_queue, mandelbrot_cl, work_dims, NULL,
		global_work_size, local_work_size, 0, NULL, NULL);
	ret = clEnqueueReadBuffer(command_queue, mem_array, CL_TRUE, 0, buffer_size, mandelbrot, 0, NULL, NULL);
	
	// Cleaning up OpenCL environment
	clFlush(command_queue);
	clFinish(command_queue);
	clReleaseKernel(mandelbrot_cl);
	clReleaseProgram(program);
	clReleaseMemObject(mem_array);
	clReleaseCommandQueue(command_queue);
	clReleaseContext(context);
	
	// Cleaning up regular data structures
	free(global_work_size);
	free(local_work_size);
	free(source_string);

	clock_t end = clock();
	double cpu_time = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("%lf", cpu_time);

}

int main()
{
	real_t xmin = 0;
	real_t xmax = 400;
	real_t ymin = 0;
	real_t ymax = 400;
	int n = 0;
	int max_iter = 1000;
	int *mandelbrot = {};

	Mandelbrot(xmin, xmax, ymin, ymax, n, 
	     max_iter, mandelbrot);

	free(mandelbrot);
	return 0;
}