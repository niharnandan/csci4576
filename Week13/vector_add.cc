/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @author Paramjot Singh <Paramjot.Singh@colorado.edu>
 * @revision $Revision: 23 $
 * @tags $Tags: tip $
 * @date $Date: Mon Oct 30 17:52:26 2017 -0600 $
 */

#include <iostream>
#include <cstdlib>
#include <stdio.h>

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#define MAX_SOURCE_SIZE (0x100000)
#define CHK(expr) { cl_int _ret=expr; if(_ret){ std::cerr<<"Function call in "<<__FILE__<<", line "<<__LINE__<<" failed with error="<<_ret<<std::endl; abort();}}

void prepare_cmd_queue(cl_device_id &device_id, cl_context &context, cl_command_queue &queue){

    // Get platform and device information
    cl_platform_id platform_id = NULL;
    cl_uint ret_num_devices;
    cl_uint ret_num_platforms;
    cl_int ret;
    CHK( clGetPlatformIDs(1, &platform_id, &ret_num_platforms) );
    CHK( clGetDeviceIDs( platform_id, CL_DEVICE_TYPE_ALL, 1, &device_id, &ret_num_devices) );

    // Create an OpenCL context
    context = clCreateContext( NULL, 1, &device_id, NULL, NULL, &ret); CHK( ret );

    // Create a command queue
    queue = clCreateCommandQueue(context, device_id, 0, &ret); CHK( ret );
}

void load_program(const char *fname, const cl_device_id device_id, const cl_context &context, cl_program &program, cl_kernel &kernel){

    // Load the kernel source code into the array source_str
    FILE *fp;
    char *source_str;
    size_t source_size;
    cl_int ret;

    fp = fopen(fname, "r");
    if (!fp) {
        std::cerr<<"Failed to load kernel from "<<fname<<std::endl;
        abort();
    }

    source_str = (char*) malloc(MAX_SOURCE_SIZE);
    source_size = fread( source_str, 1, MAX_SOURCE_SIZE, fp);
    fclose( fp );

    // Create a program from the kernel source
    program = clCreateProgramWithSource(context, 1,
        (const char **) &source_str, (const size_t *) &source_size, &ret); CHK( ret );

    // Build the program
    CHK( clBuildProgram(program, 1, &device_id, NULL, NULL, NULL) );

    // Create the OpenCL kernel
    kernel = clCreateKernel(program, "vector_add", &ret); CHK( ret );
}

int main(void) {

    // Prepare OpenCL
    cl_device_id device_id = NULL;
    cl_context context;
    cl_command_queue queue;
    prepare_cmd_queue(device_id, context, queue);

    cl_program program;
    cl_kernel vec_add;
    load_program("vector_add_kernel.cl", device_id, context, program, vec_add);

    // Example setup
    const size_t list_sz=1024;
    int *A = (int*) malloc(sizeof(int)*list_sz);
    int *B = (int*) malloc(sizeof(int)*list_sz);
    int *C = (int*) malloc(sizeof(int)*list_sz);

    for(size_t ii = 0; ii < list_sz; ++ii) {
        A[ii] = ii;
        B[ii] = list_sz - ii;
    }

    // Create memory buffers on the device for data
    cl_int ret;
    size_t byte_sz = list_sz*sizeof(int);
    cl_mem a_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY , byte_sz, NULL, &ret); CHK( ret );
    cl_mem b_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY , byte_sz, NULL, &ret); CHK( ret );
    cl_mem c_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, byte_sz, NULL, &ret); CHK( ret );

    // Set the arguments of the kernel vector_add
    CHK( clSetKernelArg(vec_add, 0 /* arg index */, sizeof(cl_mem), (void*) &a_buffer) );
    CHK( clSetKernelArg(vec_add, 1 /* arg index */, sizeof(cl_mem), (void*) &b_buffer) );
    CHK( clSetKernelArg(vec_add, 2 /* arg index */, sizeof(cl_mem), (void*) &c_buffer) );

    // Copy the lists to the buffer
    CHK( clEnqueueWriteBuffer(queue, a_buffer, CL_TRUE, 0, byte_sz, A, 0, NULL, NULL) );
    CHK( clEnqueueWriteBuffer(queue, b_buffer, CL_TRUE, 0, byte_sz, B, 0, NULL, NULL) );

    // Execute the OpenCL kernel on the list
    size_t global_item_size = list_sz; // Process the entire lists
    size_t local_item_size  = 64;         // Process in groups of 64
    CHK( clEnqueueNDRangeKernel(queue, vec_add, 1, NULL,
            &global_item_size, &local_item_size, 0, NULL, NULL) );

    // Read the memory buffer C on the device to the local variable C
    CHK( clEnqueueReadBuffer(queue, c_buffer, CL_TRUE, 0, byte_sz, C, 0, NULL, NULL) );

    // Display the result to the screen
    for(size_t ii = 0; ii < list_sz; ++ii)
        std::cout<<A[ii]<<" + "<<B[ii]<<" = "<<C[ii]<<std::endl;

    // Shutting down and clean up
    CHK( clFlush(queue) );
    CHK( clFinish(queue) );
    CHK( clReleaseKernel(vec_add) );
    CHK( clReleaseProgram(program) );
    CHK( clReleaseMemObject(a_buffer) );
    CHK( clReleaseMemObject(b_buffer) );
    CHK( clReleaseMemObject(c_buffer) );
    CHK( clReleaseCommandQueue(queue) );
    CHK( clReleaseContext(context) );

    free(A);
    free(B);
    free(C);

    return 0;
}
