__kernel void vector_add(__global const int *A, __global const int *B, __global int *C) {

    // Get the index of the current work item
    int idx = get_global_id(0);
    C[idx] = A[idx] + B[idx];
}
